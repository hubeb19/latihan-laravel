<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran</title>
</head>
<body>
    <h1>Buat account baru!</h1>
    <h3>Sign Up Form</h3>
    <!-- op form sec -->
    <form action="{{url('welcome')}}" method="POST">
        @csrf
        <label>First name:</label> <br><br>
        <input type="text" name="fname"> <br><br>

        <label>Last name:</label> <br>
        <input type="text" name="lname"> <br><br>

        <label>Gender:</label> <br><br>
        <input type="radio" name="gender" >Male<br>
        <input type="radio" name="gender" >Female<br>
        <input type="radio" name="gender" >Other<br><br>

        <label >Nationality:</label> <br><br>
        <select name="natio" >
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
            <option value="singaporean">Singaporean</option>
        </select><br><br>

        <label>Language Spoken:</label> <br><br>
        <input type="checkbox"  >Bahasa indonesia<br>
        <input type="checkbox"  >English<br>
        <input type="checkbox"  >other <br><br>



        <label >Bio</label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
        <button type="submit"> sign up</button>
    </form>
    <!-- ed form sec -->

</body>
</html>
