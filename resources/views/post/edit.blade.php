@extends('template.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>form edit pertanyaan</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->

    <div class="container-fluid">
            <div class="card">
                <form role="form" method="POST" action="/pertanyaan/{{$edit->id}}">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                      <div class="form-group">
                        <label for="judul">Title</label>
                        <input type="text" class="form-control" id="Title" value="{{old('judul',$edit->judul)}}" name="title">
                      </div>
                      <div class="form-group">
                        <label for="isi">pertanyaan</label>
                        <input type="text" class="form-control" id="isi" value="{{old('judul',$edit->isi)}}" name="isi">
                      </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                  </form>

            </div>
            <!-- form start -->

    </div>
    {{-- containerfluidend --}}
</section>
@endsection
