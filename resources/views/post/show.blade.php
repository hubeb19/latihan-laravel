@extends('template.master')
@section('content')
<div class="container-fluid">

    <div class="card">
        <h1 class="card-title">{{$seeIsi->judul}}</h1>
        <div class="card-body">
            <p>{{$seeIsi->isi}}</p>
        </div>
    </div>
</div>
@endsection
