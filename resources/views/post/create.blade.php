@extends('template.master')

@section('content')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>form create pertanyaan</h1>
        </div>
      </div>
    </div><!-- /.container-fluid -->

    <div class="container-fluid">
            <div class="card">
                <form role="form" method="POST" action="/pertanyaan">
                    @csrf
                    <div class="card-body">
                      <div class="form-group">
                        <label for="judul">Title</label>
                        <input type="text" class="form-control" id="Title" placeholder="Masukkan judul" name="title">
                      </div>
                      <div class="form-group">
                        <label for="isi">pertanyaan</label>
                        <input type="text" class="form-control" id="isi" placeholder="Masukkan pertanyaan" name="isi">
                      </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
            </div>
            <!-- form start -->

    </div>
    {{-- containerfluidend --}}
</section>
@endsection

@push('scripts')
@endpush
