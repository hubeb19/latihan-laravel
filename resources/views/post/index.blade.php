@extends('template.master')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>id</th>
          <th>judul </th>
          <th>isi pertanyaan</th>
          <th>Aksi</th>

        </tr>
        </thead>
        <tbody>
            @foreach($seeTable as $key => $seeData)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$seeData->judul}} </td>
                <td>{{$seeData->isi}}</td>
                <td style="display: flex">
                    <a href="/pertanyaan/{{$seeData->id}}/show" class="btn btn-primary btn-small">show</a>
                    <a href="/pertanyaan/{{$seeData->id}}" class="btn btn-secondary btn-small">edit</a>
                    <form action="/pertanyaan/{{$seeData->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-small">
                    </form>
                </td>

            </tr>
            @endforeach
        </tbody>

      </table>
    </div>
    <!-- /.card-body -->
  </div>
 <a href="{{url('/pertanyaan/create')}}">Buat pertanyaan</a>
</div>
@endsection

@push('scripts')
<script src="{{asset('plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script>
$(function () {
  $("#example1").DataTable();
});
</script>
@endpush
