<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class pertanyaanController extends Controller
{
   public function create(){
    return view('post.create');
   }

   public function store(Request $request){
    $validatedData= $request->validate([
        "title"=>'required',
        "isi"=>'required'
    ]);
    $query=DB:: table('pertanyaan')->insert([

        "judul"=> $request['title'],
        "isi"=>$request['isi']]);
        return redirect('/pertanyaan');

   }

   public function index(){
       $seeTable=DB:: table('pertanyaan')->get();
    return view('post.index', compact('seeTable'));
   }

   public function show($id){
       $seeIsi=DB::table('pertanyaan')->where('id',$id)->first();
    return view('post.show',compact('seeIsi'));
   }

   public function edit($id){
    $edit=DB::table('pertanyaan')->where('id',$id)->first();
    return view('post.edit',compact('edit'));

   }

   public function update(Request $request,$id){
    $update= DB:: table('pertanyaan')->where('id',$id)->update([
        "judul"=> $request['title'],
        "isi"=>$request['isi'] ]);
    return redirect ('/pertanyaan',compact('update'));
   }

   public function destroy($id){
       $delete= DB:: table('pertanyaan')->where('id',$id)->delete();
    return redirect('/pertanyaan',compact('delete'));
   }





}
