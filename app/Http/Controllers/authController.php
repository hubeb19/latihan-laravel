<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authController extends Controller
{
    function register() {
        return view('register');
    }

    function greet(Request $request) {
        $namadepan=$request->fname;
        $namabelakang=$request->lname;
        return view('welcome',compact('namadepan','namabelakang') );
    }

    function dataTables(){
        return view('datatables');
    }
}
